package main

import (
	"bitbucket.org/yossiou/apple-to-spotify/pkg/server"
)

func main() {
	server.InitServer()
}
