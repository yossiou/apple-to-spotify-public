module bitbucket.org/yossiou/apple-to-spotify

go 1.14

require (
	github.com/confluentinc/confluent-kafka-go v1.4.2
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.7.0
	go.mongodb.org/mongo-driver v1.4.1
)
